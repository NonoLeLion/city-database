from flask import Flask, render_template, redirect, request, flash, jsonify
from flask_mysqldb import MySQL, MySQLdb

app = Flask(__name__)

conf = open('config.conf')
res = conf.readlines()[:4]

app.config['MYSQL_HOST'] = res[0][6:-2]
app.config['MYSQL_USER'] = res[1][6:-2]
app.config['MYSQL_PASSWORD'] = res[2][10:-2]
app.config['MYSQL_DB'] = res[3][10:-2]
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'
mysql = MySQL(app)

# get field from database
@app.route('/')
def index():
    cur = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
    result = cur.execute("SELECT ville_id, ville_nom, Substring(ville_code_postal, 1, 5) as ville_code_postal FROM villes_france_free")
    ville_nom = cur.fetchall()
    return render_template('index.html', ville_nom=ville_nom)


@app.route("/ajax_add", methods=["POST", "GET"])
def ajax_add():
    cursor = mysql.connection.cursor()
    cur = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
    if request.method == 'POST':
        txtid = request.form['txtid']
        txtnom = request.form['txtnom']
        txtcodepostal = request.form['txtcodepostal']
        print(txtid)
        if txtid == '':
            msg = 'Please input an ID'
        elif txtnom == '':
           msg = 'Please input a city name'
        elif txtcodepostal == '':
           msg = 'Please input a postal code'
        else:
            cur.execute("INSERT INTO villes_france_free (ville_id,ville_nom,ville_code_postal) VALUES (%s,%s,%s)", [
                        txtid, txtnom, txtcodepostal])
            mysql.connection.commit()
            cur.close()
            msg = 'New record created successfully'
    return jsonify(msg)

@app.route("/ajax_delete", methods=["POST", "GET"])
def ajax_delete():
    cursor = mysql.connection.cursor()
    cur = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
    if request.method == 'POST':
        getid = request.form['ville_id']
        print("id: " + getid)
        cur.execute(
            'DELETE FROM villes_france_free WHERE ville_id = {0}'.format(getid))
        mysql.connection.commit()
        cur.close()
        msg = 'Record deleted successfully'
    return jsonify(msg)


if __name__ == "__main__":
    app.run(debug=True)
