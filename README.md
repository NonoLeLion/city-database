# Infos ⛔
This API only work with [this database](https://sql.sh/ressources/sql-villes-france/villes_france.sql).<br>
I used [MySQL](https://www.mysql.com/) for the database management.

## Content 🌟
- [Infos](#infos-⛔)
- [Setup](#setup-🔧)
    - [Database](#database)
    - [Python libs](#python-libs)
    - [Config file](#config-file)
- [Things to Improve](#things-to-improve-🤔)

## Setup 🔧
### Database
First create a new database in your SQL prompt with:
```sql
create database name_of_the_database;
```
And finaly you can set [the database](https://sql.sh/ressources/sql-villes-france/villes_france.sql) by typing the following commande in your shell:
```sh
mysql -u root -p name_of_the_database < path_to_the_file/villes_france.sql
```
### Python libs
For python dependencies use:
```sh
pip install -r requirements.txt
```
in the project directory.<br>

In case it does not work here is the list of dependencies:<br>
https://github.com/alexferl/flask-mysqldb<br>
https://github.com/pallets/flask
### Config file
In the config.conf file you can set the information, that will allow you to connect to your database.<br>
Like so:
```py
host="localhost"
user='root'
password='pass'
database='villes_tests'

# put your infos between '' or ""
```
### Run the code !
To run the code simply run the app.py file with:
```sh
python app.py
```
and go to the adress diplay in the console.
## Things to improve 🤔
- When you add a new city, you need to refresh the page, so that the city is displayed correctly in the table.
- There is a high loading time for a big database like [this](https://sql.sh/ressources/sql-villes-france/villes_france.sql).
- You must modify the code to add rows or to implement another database.
- The CSS 👀.